<?php
# Instanciamos la clase de pruebas
use PHPUnit\Framework\TestCase;
use App\Primer;

# Definimos la clase para realizar las pruebas
class PrimerTest extends TestCase
{
    # Definimos el método para probar render
    public function test_respuesta()
    {
        # Instaciamos o creamos el obajeto en base a la clase
        $prueba = new Primer();

        # Realizamos la comprobación
        $this->assertEquals($prueba->saludo(), "HOLA TEST");
        #Verificamos una suma de dos valores
        $this->assertEquals($prueba->suma(25,3), 25+3);
    }

    public function test_valor()
    {
        $prueba = new Primer();

        $this->assertEquals($prueba->valor(), 25);
    }

    public function test_suma()
    {
        $sumar = Primer::suma(2,3);

        $this->assertEquals($sumar,5);
    }
}