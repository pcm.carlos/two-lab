<?php

namespace App;

class Primer{
    # Creamos el métodp saludo
    public function saludo()
    {
        # Retornamos el saludo
        return "HOLA TEST";
    }

    public function valor()
    {
        # Retornamos el valor
        return 25;

    }

    public static function suma($valor1,$valor2)
    {
       
        $resultado = $valor1 + $valor2;
        return $resultado;
    }

}

